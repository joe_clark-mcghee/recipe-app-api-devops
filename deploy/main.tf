terraform {
  backend "s3" {
    bucket         = "jcm-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  // The terraform workspace is set using command `terraform workspace new <name>`.
  prefix = "${var.prefix}-${terraform.workspace}"
  // We will use these common_tags to tag all services on AWS so we can locate them in the console.
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    // Managed by TF so don't edit in the console.
    ManagedBy = "Terraform"
  }
}

data "aws_region" "current" {}