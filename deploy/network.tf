// define the VPC that our public and private subnet will exist in.
resource "aws_vpc" "main" {
  // The subnet mask of `/16` gives us access to 65534 hosts.
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-vpc")
  )
}

// define the internet gateway that all traffic will be routed though.
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

#######################################################
# Public subnets - Inbound and outbound internet access
#######################################################

################### Public a ##########################

// NOTE: Public IPs span 1 -> 9. Private IPs begin at 10.

resource "aws_subnet" "public_a" {
  // The subnet mask of `/24` gives us access to 254 hosts.
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  // There is an `a` and `b` availability zone per region. We define and `a` and `b` region so
  // divert traffic if one goes done. We also need to define two availability zones for the load
  // balancer to work.
  availability_zone = "${data.aws_region.current.name}a"
  vpc_id            = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

// Define the route table within the VPC.
resource "aws_route_table" "public_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

// Associate the route table with subnet `public_a`.
resource "aws_route_table_association" "public_a" {
  route_table_id = aws_route_table.public_a.id
  subnet_id      = aws_subnet.public_a.id
}

// Define a route in the route table.
// The internet gateway is the interface between the subnet and the public internet so we need
// to define a route that allows traffic from the public facing internet to our subnet.
resource "aws_route" "public_internet_access_a" {
  route_table_id = aws_route_table.public_a.id
  // Allow out bound traffic to all possible IP's i.e. the public facing internet.
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

// Define an elastic IP that we will assign to the NAT gateway.
resource "aws_eip" "public_a" {
  // The eip is defined within the VPC.
  vpc = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

// Define a NAT gateway used for outbound traffic from the private subnet.
resource "aws_nat_gateway" "public_a" {
  // Assign the eip to the gateway.
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

################### Public b ###########################

resource "aws_subnet" "public_b" {
  // We increment the network by 1.
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_route_table" "public_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}

resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_eip" "public_b" {
  vpc = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_nat_gateway" "public_b" {
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

##################################################
# Private subnet - Outbound internet access only #
##################################################

################### Private a #####################

resource "aws_subnet" "private_a" {
  // We start the private subnets at 10. This allows out public subnets to grow as required.
  cidr_block        = "10.1.10.0/24"
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id = aws_route_table.private_a.id
  // We point the route at the NAT gateway which only allows outbound traffic.
  nat_gateway_id         = aws_nat_gateway.public_a.id
  destination_cidr_block = "0.0.0.0/0"
}

################### Private b #####################

resource "aws_subnet" "private_b" {
  // We increment the network by 1.
  cidr_block        = "10.1.11.0/24"
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}

resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private_b.id
  nat_gateway_id         = aws_nat_gateway.public_b.id
  destination_cidr_block = "0.0.0.0/0"
}
