resource "aws_lb" "api" {
  name = "${local.prefix}-main"
  // There are two main types of load balancer; application (used at the application level for
  // http) and network (used at the network level for tcp).
  load_balancer_type = "application"
  // The load balancer has to be in the public subnet as we want it to me accessible from the
  // public internet.
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

// The target group is the group of servers the load balancer can forward requests too.
// We add ecs tasks to the target group by their IP address.
resource "aws_lb_target_group" "api" {
  name = "${local.prefix}-api"
  // We forward the request using HTTP, this means we dont need to get SSL certs for the
  // internal network.
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  // The port we forward request on to. In this case we forward the requests to the proxy.
  port = 8000

  // ecs makes a request to this address regularly to see if the app is healthy. If any thing
  // other than a 200 is returned ecs tries to restart the app.
  health_check {
    path = "/admin/login/"
  }
}

// The listener accepts the request for the load balancer and then forward the request to the lb
// target group which in turn forwards the request to the app. In the way, the listener is the
// entry point into the load balancer.
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  // The port and the HTTP protocol we accept requests on.
  port     = 80
  protocol = "HTTP"

  // If a HTTP request is made we redirect the request to the HTTPS listener. This mean that a
  // user would not be able to make insecure connections to the app.
  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

// Listen for HTTPS requests this allows us to make HTTPS requests to the load balancer.
resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_lb.api.arn
  port              = 443
  protocol          = "HTTPS"

  // As this is HTTPS we need to define the cert we are using to secure the traffic.
  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}



// We define what inbound and outbound traffic the lb accepts.
resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  // Accept all HTTP connections from the public internet.
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Accept all HTTPS connections from the public internet.
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Outbound traffic from the load balance is sent on port 8000, i.e. the lb forwards requests
  // to the app on port 8000.
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}
