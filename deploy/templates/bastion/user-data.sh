#!/bin/bash

# Update yum packet manager.
sudo yum update -y
# Install docker.
sudo amazon-linux-extras install -y docker
# Enable docker on the machine and then start it.
sudo systemctl enable docker.service
sudo systemctl start docker.service
# The bastion server is built from a standard linux AMI. The default user when this instance
# type is created is ec2-user.
# We give the default ec2-user permissions to interact with docker.
sudo usermod -aG docker ec2-user

