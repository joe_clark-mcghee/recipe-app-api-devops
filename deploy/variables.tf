variable "prefix" {
  // recipe app api devops
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "joeclarkmcghee@yahoo.co.uk"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

// We set this key in the AWS console within the EC2 dashboard.
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "205104583549.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for reverse proxy"
  default     = "205104583549.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  // Set from variables defined in GitLab
  description = "Secrete key for django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "sql-joe.co.uk"
}

// define the subdomain names.
// The variable subdomain is of type map i.e. key/value pairs. The map is defined in the default
// block.
variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}


