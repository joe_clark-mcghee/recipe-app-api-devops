// Pull data about a particular "thing" from aws. In this case we pull data about a specific
// aws_ami. We could just as easily pull data about an ec2 instance that we defined, we would
// just need to specify "aws_instance" as the data type. Check out the TF docs for more.
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name = "name"
    // Get the amazon linux ami
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}



// Define IAM role for the bastion instance.
resource "aws_iam_role" "bastion" {
  name = "${local.prefix}-bastion"
  // Instance profiles act as a container for an IAM role on a running EC2 instance. We attach
  // specific policies to a role on instance creation (See aws_iam_role_policy_attachment).
  // `AssumeRole` in `instance-profile-policy.json` gives us temporary creds such that the
  // instance is able to make api calls to the ec2 related endpoints, ultimately to attach a
  // policy.
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

// Attach polices to role.
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role = aws_iam_role.bastion.name
  // Attach the AmazonEC2ContainerRegistryReadOnly policy to the IAM role. We use the unique arn
  // number to define the policy.
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

// Define a profile that will be assigned to bastion when it launches.
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  // Get the id of the ami we want to be created.
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  // Assign the instance profile to bastion.
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  // The script passed to user_data is run when the instance is created.
  // We define a script that installs docker on to the bastion instance. We install this to make
  // performing admin tasks easier when we shell on to bastion.
  user_data = file("./templates/bastion/user-data.sh")
  // Assign the ssh key to our bastion instance.
  key_name = var.bastion_key_name
  // Launch the bastion server on the public facing subnet.
  subnet_id = aws_subnet.public_a.id

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  // Tag the bastion instance so we can easily see it in aws
  // We use merge to combine all the common_tags and then add a new tag "Name", using `map`.
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

// Define a security group that we will attach to bastion. We only expose ports related to HTTP
// (80), HTTPS (443), SSH (22) and postgres traffic (5432)
resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound traffic"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  // Only allow inbound access via SSH access (port 22)
  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    // You could specify specific IP address here (essentially white listing them) so that only
    // inbound traffic from these ips would be accepted.
    cidr_blocks = ["0.0.0.0/0"]
  }

  // We need to allow bastion to make outbound requests we do this by exposing the HTTP/S ports.
  // Outbound HTTPS.
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Outbound HTTP.
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Outbound db traffic to postgres.
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}