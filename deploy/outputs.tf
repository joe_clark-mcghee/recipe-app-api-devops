// Define variables that will be output to the terminal once the deploy has finished.

// Output the ip of the db so we can connect to it for debugging purposes.
output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}
