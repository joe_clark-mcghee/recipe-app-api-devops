// Create a data resource to retrieve our zone name in AWS.
data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

// Create a record within route53 which will allow us to create the subdomains.
resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  // lookup is a terraform function that operates on a map.
  name = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  type = "CNAME"
  ttl  = "300"

  records = [aws_lb.api.dns_name]
}

// We create a new cert in AWS certificate manager for our domain name.
resource "aws_acm_certificate" "cert" {
  domain_name       = aws_route53_record.app.fqdn
  validation_method = "DNS"

  tags = local.common_tags

  // TF recommend that you add this value when creating a cert.
  lifecycle {
    create_before_destroy = true
  }
}

// This is how we programmatically validate that we own the domain and are allowed to create
// SSL certs for it.
resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]
  ttl = "60"
}

// This "resource" is not persistent. It is only used to trigger the validation process of in TF.
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
