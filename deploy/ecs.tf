resource "aws_ecs_cluster" "main" {
  // We have a cluster for each workspace, dev and prod respectively.
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

########################################## IAM #######################################

/*
We need to define two policies, one that the task uses when it is building the containers and
another other that the containers will be assigned when they are built and running.

build policy:
The build task needs permissions to pull down images and build the containers. We define the
allowed actions for this policy in task-exec-role.json.

run time policy:
Assign permissions that the containers need to function when they are up and running.
*/

// Policy with allowed actions required for the task to build the containers.
resource "aws_iam_policy" "task_execution_role_policy" {
  name = "${local.prefix}-task-exec-role-policy"
  // Place the path at the route of the policy dir in AWS
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

// IAM role with the appropriate build permissions.
// Define the IAM role and set assume_role_policy so that we can attach
// task_execution_role_policy to it.
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

// Attach the policy to the role
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

// IAM role with the appropriate run time permissions.
// Define the IAM role and set assume_role_policy so that we can attach other policies to it.
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

#############################################################################################

// Define a log group that we can point to
resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

// The container-definitions.json.tpl is a template file used to define the API and proxy
// containers.
data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  // Populate the template file with missing values.
  vars = {
    app_image         = var.ecr_image_api
    proxy_image       = var.ecr_image_proxy
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    // We set the allowed hosts to our route53 domain names.
    allowed_hosts = aws_route53_record.app.fqdn
  }
}

resource "aws_ecs_task_definition" "api" {
  family = "${local.prefix}-api"
  // Call .rendered to actually populate the template.
  container_definitions    = data.template_file.api_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  // We assign the `task_execution_role` to the build task.
  execution_role_arn = aws_iam_role.task_execution_role.arn
  // We assign the `app_iam_role` to the run time task.
  task_role_arn = aws_iam_role.app_iam_role.arn
  volume {
    name = "static"
  }

  tags = local.common_tags
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  // Allow outbound traffic so the containers can do things like pull images.
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Allow outbound traffic from the service to the database.
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.public_a.cidr_block,
      aws_subnet.public_b.cidr_block,
    ]
  }

  // Only allow inbound traffic, on port 8000, from the load balancer.
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"

    security_groups = [
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  //  The number of tasks we wish to run inside the service.
  desired_count = 1
  launch_type   = "FARGATE"

  // As the service does not accept connections from the public internet we place the ECS in the
  // private subnet.
  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  // We tell ECS to register new "proxy" tasks within the target group.
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  // We ensure that the load balancer listener is deployed in AWS before we start the "api"
  // service.
  depends_on = [aws_lb_listener.api_https]
}
